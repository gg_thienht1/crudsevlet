package util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import bean.BookEntity;


public class MyData {
	public static List<BookEntity> viewBook() throws SQLException, ClassNotFoundException{
		Connection conn = MyConnection.getConnection();
		String sql= "select b.nameBook, b.description, b.author from book b";
		PreparedStatement prst=(PreparedStatement) conn.prepareStatement(sql);
		ResultSet rs= prst.executeQuery();
		List<BookEntity> listBook= new ArrayList<BookEntity>();
		while(rs.next()) {
			String names= rs.getString("nameBook");
			String descriptions= rs.getString("description");
			String authors=rs.getString("author");
			BookEntity book= new BookEntity();
			book.setName(names);
			book.setDescription(descriptions);
			book.setAuthor(authors);
			listBook.add(book);
		}
		return listBook;
	}
	public static void createBook(BookEntity book) throws SQLException {
		Connection conn= MyConnection.getConnection();
		String sql= "insert into book(nameBook,description, author) values (?,?,?)";
		PreparedStatement pr=(PreparedStatement) conn.prepareStatement(sql);
		pr.setString(1, book.getName());
		pr.setString(2, book.getDescription());
		pr.setString(3, book.getAuthor());
		pr.executeUpdate();
	}
	public static void deleteBook(String id) throws SQLException {
		Connection conn= MyConnection.getConnection();
		String sql="delete from book where idbook=?";
		PreparedStatement pr=(PreparedStatement) conn.prepareStatement(sql);
		pr.setString(1, String.valueOf(id));
		pr.executeQuery();
	}
	public static void updateBook(BookEntity book) throws SQLException {
		Connection conn = MyConnection.getConnection();
		String sql="update book set nameBook=?,description=?,author=?";
		PreparedStatement pr=(PreparedStatement) conn.prepareStatement(sql);
		pr.setString(1, book.getName());
		pr.setString(2, book.getDescription());
		pr.setString(3, book.getAuthor());
		pr.executeUpdate();
		
	}
}
