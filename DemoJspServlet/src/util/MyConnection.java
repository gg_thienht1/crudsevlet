package util;

import java.sql.DriverManager;

import com.mysql.jdbc.Connection;

public class MyConnection {
 
	public static Connection getConnection() {
		Connection cons = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cons = (Connection) DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/book", "root", "123456");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cons;
	}
}
